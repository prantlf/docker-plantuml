clean ::
	docker image rm plantuml prantlf/plantuml registry.gitlab.com/prantlf/docker-plantuml

lint ::
	docker run --rm -i \
		-v "${PWD}"/.hadolint.yaml:/bin/hadolint.yaml \
		-e XDG_CONFIG_HOME=/bin hadolint/hadolint \
		< Dockerfile

build ::
	docker build -t plantuml .
	docker tag plantuml prantlf/plantuml
	docker tag plantuml prantlf/plantuml:jre8-alpine

run-help ::
	docker run --rm -it plantuml

run-example ::
	docker run --rm -it -v ${PWD}:/work -w /work plantuml -v examples/hello-sequence.puml

login ::
	docker login --username=prantlf
	docker login registry.gitlab.com --username=prantlf

pull ::
	docker pull prantlf/plantuml
	docker pull registry.gitlab.com/prantlf/docker-plantuml

push ::
	docker push prantlf/plantuml
	docker tag prantlf/plantuml prantlf/plantuml:jre8-alpine
	docker push prantlf/plantuml:jre8-alpine
	docker tag registry.gitlab.com/prantlf/docker-plantuml registry.gitlab.com/prantlf/docker-plantuml:jre8-alpine
	docker push registry.gitlab.com/prantlf/docker-plantuml:jre8-alpine
